# core-foundry

Bibliothèque pour Foundry VTT

---

<details>
<summary>Versions</summary>

* **2024.2.1**
	> * [x] Compatibilité foundry v12
	> * [x] Ajouter le lien du tableau des tickets gitlab

* **2024.1.3**
	> * [x] Réorganisation et refactoring du code
	> * [x] Finalisation de la classe Localisation avec ajout du fichier langs
	> * [x] Ajout de la classe PjAbstractSheet.mjs héritant de ActorSheet
	> * [x] Ajout des templates réutilisables
	> * [x] Ajout classe Filtres 
	> * [x] Compatibilité foundry v11

* **2023.2.1**
	> * [x] Amélioration de la classe Localisation
	> * [x] Maintenance corrective

* **2023.1.2**
	> * [x] Maintenance corrective

</details>